package lsg;

import javafx.application.Application;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lsg.armor.ArmorItem;
import lsg.armor.MetalChestPlate;
import lsg.bags.Collectible;
import lsg.buffs.rings.Ring;
import lsg.characters.Character;
import lsg.characters.Hero;
import lsg.characters.Zombie;
import lsg.consumables.Consumable;
import lsg.consumables.food.SuperBerry;
import lsg.exceptions.*;
import lsg.graphics.CSSFactory;
import lsg.graphics.CollectibleFactory;
import lsg.graphics.ImageFactory;
import lsg.graphics.LootSystem;
import lsg.graphics.panes.AnimationPane;
import lsg.graphics.panes.CreationPane;
import lsg.graphics.panes.HUDPane;
import lsg.graphics.panes.TitlePane;
import lsg.graphics.widgets.characters.renderers.CharacterRenderer;
import lsg.graphics.widgets.characters.renderers.HeroRenderer;
import lsg.graphics.widgets.characters.renderers.ZombieRenderer;
import lsg.graphics.widgets.skills.SkillAction;
import lsg.graphics.widgets.skills.SkillBar;
import lsg.weapons.MetalSword;
import lsg.weapons.Sword;
import lsg.weapons.Weapon;

public class LearningSoulsGameApplication extends Application{

    public static final String TITLE = "Learning Souls Game" ;

    public static final double DEFAULT_SCENE_WIDTH = 1200 ;
    public static final double DEFAULT_SCENE_HEIGHT = 800 ;

    private Scene scene ;
    private AnchorPane root;

    private TitlePane gameTitle ;
    private CreationPane creationPane ;
    private AnimationPane animationPane ;
    private HUDPane hudPane;

    private String heroName ;
    private Hero hero;
    private HeroRenderer heroRenderer;
    private Zombie zombie;
    private ZombieRenderer zombieRenderer;

    private SkillBar skillBar;
    private BooleanProperty heroCanPlay = new SimpleBooleanProperty(false);
    private IntegerProperty score = new SimpleIntegerProperty();

    private Collectible loot;

    @Override
    public void start(Stage stage) throws Exception{
        stage.setTitle(TITLE);
        root = new AnchorPane() ;
        scene = new Scene(root, DEFAULT_SCENE_WIDTH, DEFAULT_SCENE_HEIGHT);

        stage.setResizable(false);
        stage.setScene(scene);
        buildUI() ;
        addListeners() ;

        stage.show();

        startGame() ;
    }

    private void addListeners(){
        creationPane.getNameField().setOnAction((event -> {
            heroName = creationPane.getNameField().getText() ;
            System.out.println("Nom du héro : " + heroName);
            if(!heroName.isEmpty()){
                root.getChildren().remove(creationPane);
                gameTitle.zoomOut((event1 -> play()));
            }
        }));
    }

    private void startGame(){
        gameTitle.zoomIn((event -> {
            creationPane.fadeIn((event1 -> {
                ImageFactory.preloadAll((() -> {
                    System.out.println("Pré-chargement des images terminé") ;
                })) ;
            }));
        }));
    }

    private void play(){
        root.getChildren().add(animationPane) ;
        root.getChildren().addAll(hudPane) ;
        createHero();
        createSkills();
        createMonster((event -> {
            hudPane.getMessagePane().showMessage("FIGHT!");
            heroCanPlay.setValue(true);
        }));
        hudPane.scoreProperty().bind(score);
        displayButtons();
    }


    private void buildUI(){
        scene.getStylesheets().add(CSSFactory.getStyleSheet("LSG.css")) ;

        gameTitle = new TitlePane(scene, LearningSoulsGameApplication.TITLE) ;
        AnchorPane.setTopAnchor(gameTitle, 0.0);
        AnchorPane.setLeftAnchor(gameTitle, 0.0);
        AnchorPane.setRightAnchor(gameTitle, 0.0);
        root.getChildren().addAll(gameTitle) ;

        creationPane = new CreationPane() ;
        creationPane.setOpacity(0);
        AnchorPane.setTopAnchor(creationPane, 0.0);
        AnchorPane.setLeftAnchor(creationPane, 0.0);
        AnchorPane.setRightAnchor(creationPane, 0.0);
        AnchorPane.setBottomAnchor(creationPane, 0.0);
        root.getChildren().addAll(creationPane) ;

        hudPane = new HUDPane();
        AnchorPane.setTopAnchor(hudPane, 0.0);
        AnchorPane.setLeftAnchor(hudPane, 0.0);
        AnchorPane.setRightAnchor(hudPane, 0.0);
        AnchorPane.setBottomAnchor(hudPane, 0.0);

        animationPane = new AnimationPane(root) ;
    }

    private void createHero(){
        MetalChestPlate metalChestPlate = new MetalChestPlate();
        hero = new Hero(heroName);
        hero.setWeapon(new Sword());
        hero.setConsumable(new SuperBerry());
        heroRenderer = animationPane.createHeroRenderer();
        heroRenderer.goTo(animationPane.getPrefWidth()*0.5 - heroRenderer.getFitWidth()*0.65, null);
        hudPane.getHeroStatBar().getName().setText(heroName);
        hudPane.getHeroStatBar().getLifeBar().progressProperty().bind(hero.lifeRateProperty());
        hudPane.getHeroStatBar().getStamBar().progressProperty().bind(hero.stamRateProperty());
    }

    private void createMonster(EventHandler<ActionEvent> finishedHandler){
        zombie = new Zombie();
        zombieRenderer = animationPane.createZombieRenderer();
        zombieRenderer.goTo(animationPane.getPrefWidth()*0.5 - zombieRenderer.getBoundsInLocal().getWidth() * 0.15, finishedHandler);
        hudPane.getMonsterStatBar().getName().setText("Zombie");
        hudPane.getMonsterStatBar().getAvatar().setImage(ImageFactory.getSprites(ImageFactory.SPRITES_ID.ZOMBIE_HEAD)[0]);
        hudPane.getMonsterStatBar().getAvatar().setRotate(25);
        hudPane.getMonsterStatBar().getLifeBar().progressProperty().bind(zombie.lifeRateProperty());
        hudPane.getMonsterStatBar().getStamBar().progressProperty().bind(zombie.stamRateProperty());
    }

    private void createSkills(){
        this.skillBar = hudPane.getSkillBar();
        skillBar.setDisable(!heroCanPlay.getValue());
        heroCanPlay.addListener((ChangeListener) (observable, oldValue, newValue) -> skillBar.setDisable(!heroCanPlay.getValue()));

        skillBar.getTrigger(1).setImage(ImageFactory.getSprites(ImageFactory.SPRITES_ID.ATTACK_SKILL)[0]);
        skillBar.getTrigger(1).setAction(new SkillAction() {
            @Override
            public void execute() {
                heroAttack();
            }
        });

        skillBar.getTrigger(2).setImage(ImageFactory.getSprites(ImageFactory.SPRITES_ID.RECUPERATE_SKILL)[0]);
        skillBar.getTrigger(2).setAction(new SkillAction() {
            @Override
            public void execute() {
                heroRecuperate();
            }
        });

        skillBar.getTrigger(3).setImage(ImageFactory.getSprites(ImageFactory.SPRITES_ID.SMASH_SKILL)[0]);
        skillBar.getTrigger(3).setAction(new SkillAction() {
            @Override
            public void execute() {
                smash();
            }
        });

        skillBar.getTrigger(4).setImage(ImageFactory.getSprites(ImageFactory.SPRITES_ID.DOUBLE_ATTACK_SKILL)[0]);
        skillBar.getTrigger(4).setAction(new SkillAction() {
            @Override
            public void execute() {
                doubleAttack();
            }
        });

        scene.setOnKeyReleased(event -> {
            skillBar.process(event.getCode());
        });
        skillBar.getConsumableTrigger().setConsumable(hero.getConsumable());
        skillBar.getConsumableTrigger().setAction(new SkillAction() {
            @Override
            public void execute() {
                heroConsume();
            }
        });
    }

    private void characterAttack(Character aggressor, CharacterRenderer aggressorR,
                                 Character target, CharacterRenderer targetR,
                                 EventHandler<ActionEvent> finishedHandler){
        int attack = 0, damage;
        try {
            attack = aggressor.attack();
            damage = target.getHitWith(attack);
            target.setLife(target.getLife() - damage);
            aggressorR.attack(event -> {
                if (target.isAlive()) {
                    targetR.hurt(finishedHandler);
                } else {
                    targetR.die(finishedHandler);
                }
            });
        } catch (WeaponNullException | WeaponBrokenException | StaminaEmptyException e) {

            aggressorR.attack(finishedHandler);
            hudPane.getMessagePane().showMessage(e.getLocalizedMessage());
        }
    }

    private void heroAttack(){
        heroCanPlay.setValue(false);
        characterAttack(hero,heroRenderer,zombie,zombieRenderer,event -> {
            finishTurn();
        });
    }

    private void displayButtons(){
        hudPane.getArmorButton().setOnMouseClicked(event -> {
                if(hudPane.getArmorButton().isDisplay()){
                    hudPane.getArmorDisplayed().getChildren().removeAll(hudPane.getArmorDisplayed().getChildren());
                    hudPane.getArmorButton().setText("Display Armor");
                } else {
                    hudPane.getArmorButton().setText("Hide Armor");
                    displayArmor();
                }
                hudPane.getArmorButton().setDisplay(!hudPane.getArmorButton().isDisplay());
        });

        hudPane.getBagButton().setOnMouseClicked(event -> {
            if(hudPane.getBagButton().isDisplay()){
                hudPane.getBagDisplayed().getChildren().removeAll(hudPane.getBagDisplayed().getChildren());
                hudPane.getBagButton().setText("Display Bag");
            } else {
                hudPane.getBagButton().setText("Hide Bag");
                displayBag();
            }
            hudPane.getBagButton().setDisplay(!hudPane.getBagButton().isDisplay());
        });
    }

    private void displayBag(){
        try {
            for (Collectible item : hero.getBagItems()){
                ImageView itemImage = new ImageView();
                itemImage.setImage(CollectibleFactory.getImageFor(item));
                hudPane.getBagDisplayed().getChildren().add(itemImage);
                itemImage.setOnMousePressed(event -> {
                    if(event.getButton() == MouseButton.SECONDARY) {
                        try {
                            hero.pullOut(item);
                            refresh();
                        } catch (BagNullException e) {
                            hudPane.getMessagePane().showMessage(e.getLocalizedMessage());
                        }
                    } else if(event.getButton() == MouseButton.PRIMARY){
                        try {
                            equip(item);
                            refresh();
                        } catch (Exception e) {
                            hudPane.getMessagePane().showMessage(e.getLocalizedMessage());
                        }
                    }
                });

            }
        } catch (BagNullException e) {
            hudPane.getMessagePane().showMessage(e.getLocalizedMessage());
        }
    }

    private void displayArmor(){
        for (ArmorItem armorItem : hero.getArmorItems()) {
            ImageView itemImage = new ImageView();
            itemImage.setImage(CollectibleFactory.getImageFor(armorItem));
            hudPane.getArmorDisplayed().getChildren().add(itemImage);
            itemImage.setOnMouseClicked(event -> {
                try {
                    hero.unequip(armorItem);
                    refresh();
                } catch (BagFullException e) {
                    hudPane.getMessagePane().showMessage(e.getLocalizedMessage());
                }
            });
        }
    }

    private void monsterAttack(){
        characterAttack(zombie,zombieRenderer,hero,heroRenderer,event -> {
            if(hero.isAlive()){
                heroCanPlay.setValue(true);
            } else {
                gameOver();
            }
        });
    }

    private void gameOver(){
        hudPane.getMessagePane().showMessage("GAME OVER");
    }

    private void finishTurn(){
        if(zombie.isAlive()){
            monsterAttack();
        } else {
            score.setValue(score.getValue()+1);
            loot = LootSystem.randomLoot();
            ImageView lootImage = new ImageView();
            lootImage.setImage(ImageFactory.getSprites(ImageFactory.SPRITES_ID.LOOT)[0]);
            hudPane.getLootBox().getChildren().add(lootImage);
            lootImage.setOnMouseClicked(event -> {
                try {
                    hero.pickUp(loot);
                    hudPane.getLootBox().getChildren().remove(lootImage);
                    refresh();
                } catch (BagNullException | BagFullException e) {
                    hudPane.getMessagePane().showMessage(e.getLocalizedMessage());
                }
            });
            animationPane.getChildren().remove(zombieRenderer);
            createMonster(event -> monsterAttack());
        }
    }

    private void heroRecuperate(){
        heroCanPlay.setValue(false);
        hero.recuperate();
        finishTurn();
    }

    private void heroConsume(){
        heroCanPlay.setValue(false);
        try {
            hero.consume();
        } catch (ConsumeRepairNullWeaponException | ConsumeNullException | ConsumeEmptyException e) {
            hudPane.getMessagePane().showMessage(e.getLocalizedMessage());
        }
        finishTurn();
    }

    private void smash(){
        int attack = 0, damage;
        try {
            attack = hero.strongAttack();
            damage = zombie.getHitWith(attack);
            zombie.setLife(zombie.getLife() - damage);
            heroRenderer.attack(event -> {
                if (zombie.isAlive()) {
                    zombieRenderer.hurt(event1 -> finishTurn());
                } else {
                    zombieRenderer.die(event1 -> finishTurn());
                }
            });
        } catch (WeaponNullException | WeaponBrokenException | StaminaEmptyException e) {
            heroRenderer.attack(event -> finishTurn());
            hudPane.getMessagePane().showMessage(e.getLocalizedMessage());
        }
    }

    private void doubleAttack(){
        heroCanPlay.setValue(false);
        int attack = 0, damage;
        try {
            attack = hero.attack() + hero.attack();
            damage = zombie.getHitWith(attack);
            zombie.setLife(zombie.getLife() - damage);
            heroRenderer.attack(event -> {
                zombieRenderer.hurt(event1 -> {
                    heroRenderer.attack(event2 -> {
                        if (zombie.isAlive()) {
                            zombieRenderer.hurt(event3 -> finishTurn());
                        } else {
                            zombieRenderer.die(event3 -> finishTurn());
                        }
                    });
                });
            });
        } catch (WeaponNullException | WeaponBrokenException | StaminaEmptyException e) {

            heroRenderer.attack(event -> finishTurn());
            hudPane.getMessagePane().showMessage(e.getLocalizedMessage());
        }
    }

    private void refresh(){
        if(hudPane.getBagButton().isDisplay()){
            hudPane.getBagDisplayed().getChildren().removeAll(hudPane.getBagDisplayed().getChildren());
            displayBag();
        }
        if(hudPane.getArmorButton().isDisplay()){
            hudPane.getArmorDisplayed().getChildren().removeAll(hudPane.getArmorDisplayed().getChildren());
            displayArmor();
        }
    }

    private void equip(Collectible collectible) throws Exception {
        if (collectible instanceof ArmorItem){
            if(hero.getArmorItems().length < 3) {
                hero.equip((ArmorItem) collectible, hero.getArmorItems().length + 1);
            } else throw new Exception("Unequip a piece of armor before");
        } else if (collectible instanceof Ring){
            if(hero.getRings().length < 2) {
                hero.equip((Ring) collectible, hero.getRings().length + 1);
            } else throw new Exception("Unequip a ring before");
        } else if(collectible instanceof Consumable){
            hero.equip((Consumable) collectible);
            hudPane.getSkillBar().getConsumableTrigger().setConsumable((Consumable) collectible);
        } else if(collectible instanceof Weapon){
            hero.equip((Weapon) collectible);
        }
    }

    public static void main(String[] args) {launch(args);}

}
