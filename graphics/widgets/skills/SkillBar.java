package lsg.graphics.widgets.skills;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Rectangle;
import lsg.graphics.CSSFactory;

import java.sql.Array;
import java.util.LinkedHashMap;
import java.util.Map;

public class SkillBar extends HBox {

    private static LinkedHashMap<KeyCode,String> DEFAULT_BINDING = new LinkedHashMap<>();
    static {
        DEFAULT_BINDING.put(KeyCode.DIGIT1,"&");
        DEFAULT_BINDING.put(KeyCode.DIGIT2,"é");
        DEFAULT_BINDING.put(KeyCode.DIGIT3,"\"");
        DEFAULT_BINDING.put(KeyCode.DIGIT4,"'");
        DEFAULT_BINDING.put(KeyCode.DIGIT5,"(");
    }

    private SkillTrigger triggers[];
    private ConsumableTrigger consumableTrigger = new ConsumableTrigger(KeyCode.C,"c",null,null);;

    public SkillBar(){
        this.getStylesheets().add(CSSFactory.getStyleSheet("SkillTrigger.css")) ;
        this.setSpacing(10);
        this.prefHeight(110);
        this.setAlignment(Pos.CENTER);
        this.setPadding(new Insets(0,0,25,0));
        init();
    }

    private void init(){
        triggers=new SkillTrigger[DEFAULT_BINDING.size()];
        int i = 0 ;
        for(Map.Entry<KeyCode,String> entry : DEFAULT_BINDING.entrySet()){
            triggers[i] = new SkillTrigger(entry.getKey(),entry.getValue(),null,null);
            this.getChildren().add(triggers[i]);
            i++;
        }
        this.getChildren().add(new Rectangle(30,0));
        this.getChildren().add(consumableTrigger);
    }

    public SkillTrigger getTrigger(int slot){
        return triggers[slot-1];
    }

    public ConsumableTrigger getConsumableTrigger(){ return consumableTrigger; }

    public void process(KeyCode code){
        if (!this.isDisabled()) {
            if (code == KeyCode.C) {
                consumableTrigger.animate();
                consumableTrigger.getAction().execute();
            } else {
                for (SkillTrigger trigger : triggers) {
                    if (trigger.getKeyCode() == code) {
                        trigger.animate();
                        trigger.getAction().execute();
                        break;
                    }
                }
            }
        }
    }
}
