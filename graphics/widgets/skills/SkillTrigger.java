package lsg.graphics.widgets.skills;

import javafx.animation.ScaleTransition;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

public class SkillTrigger extends AnchorPane {
    private static final Duration ANIMATION_DURATION = Duration.millis(100);

    private ImageView view;
    private Label text;
    private KeyCode keyCode;
    private SkillAction action;
    private ColorAdjust desaturate;

    public Label getText() {
        return text;
    }

    public void setText(Label text) {
        this.text = text;
    }

    public KeyCode getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(KeyCode keyCode) {
        this.keyCode = keyCode;
    }

    public SkillAction getAction() {
        return action;
    }

    public void setAction(SkillAction action) {
        this.action = action;
    }

    public Image getImage(){
        return view.getImage();
    }

    public void setImage(Image image){
        view.setImage(image);
    }

    public SkillTrigger(KeyCode keyCode, String text, Image image, SkillAction action){
        this.keyCode = keyCode;
        this.text = new Label(text);
        view = new ImageView();
        this.setImage(image);
        this.action = action;
        this.desaturate = new ColorAdjust();
        this.desaturate.setSaturation(-1);
        this.desaturate.setBrightness(0.6);
        this.buildUI();
        addListeners();
    }

    private void buildUI(){
        this.getStyleClass().add("skill");
        this.text.setAlignment(Pos.CENTER);
        text.setStyle("-fx-font-size: 22px;-fx-effect: dropshadow( three-pass-box, white , 3.0, 0.8,0.0,0.0)");
        view.setFitWidth(50);
        view.setFitHeight(50);
        this.getChildren().add(view);
        AnchorPane.setTopAnchor(view,0.0);
        AnchorPane.setRightAnchor(view,0.0);
        AnchorPane.setBottomAnchor(view,0.0);
        AnchorPane.setLeftAnchor(view,0.0);
        this.getChildren().add(text);
        AnchorPane.setTopAnchor(text,0.0);
        AnchorPane.setRightAnchor(text,0.0);
        AnchorPane.setBottomAnchor(text,0.0);
        AnchorPane.setLeftAnchor(text,0.0);
    }

    public void trigger(){
        if(!this.isDisabled()){
            animate();
            if(action != null) action.execute();
        }
    }

    private void addListeners(){
        this.setOnMouseClicked((event -> {
            trigger();
        }));
        disabledProperty().addListener((ChangeListener) (observable, oldValue, newValue) -> {
            if(disabledProperty().getValue()){
                this.view.setEffect(desaturate);
            } else {
                this.view.setEffect(null);
            }
        });
    }

    public void animate(){
        ScaleTransition st = new ScaleTransition(ANIMATION_DURATION) ;
        st.setToX(1.3);
        st.setToY(1.3);
        st.setNode(this);
        st.setCycleCount(2);
        st.setAutoReverse(true);
        st.play();
    }
}
