package lsg.graphics.widgets.skills;

import javafx.beans.value.ChangeListener;
import javafx.scene.input.KeyCode;
import lsg.consumables.Consumable;
import lsg.graphics.CollectibleFactory;

public class ConsumableTrigger extends SkillTrigger{
    private Consumable consumable;

    public void setConsumable(Consumable consumable){
        this.consumable = consumable;
        this.setImage(CollectibleFactory.getImageFor(consumable));
        if(consumable != null){
            if(!this.getParent().disabledProperty().getValue()) {
                if (!consumable.isEmptyProperty().get()) this.setDisabled(false);
            }
            this.consumable.isEmptyProperty().addListener(
                    (ChangeListener) (observable, oldValue, newValue) -> this.setDisabled(true));
        }
    }

    public ConsumableTrigger(KeyCode keyCode, String text, Consumable consumable, SkillAction action) {
        super(keyCode, text, null, action);
        setConsumable(consumable);
        addListeners();
    }

    private void addListeners(){
        this.setOnMouseClicked((event -> {
            trigger();
        }));
        disabledProperty().addListener((ChangeListener) (observable, oldValue, newValue) -> {
            if(!disabledProperty().getValue()){
                if(this.consumable == null || this.consumable.isEmptyProperty().get()) this.setDisabled(true);
            }
        });
    }
}
