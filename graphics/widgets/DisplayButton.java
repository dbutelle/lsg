package lsg.graphics.widgets;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class DisplayButton extends AnchorPane {
    private Label text;
    private boolean display;

    public Label getText() {
        return text;
    }

    public void setText(String text) {
        this.text.setText(text);
    }

    public boolean isDisplay() {
        return display;
    }

    public void setDisplay(boolean display) {
        this.display = display;
    }

    public DisplayButton(String text) {
        this.text = new Label(text);
        this.display = false;
        buidUI();
    }

    private void buidUI(){
        this.setStyle("-fx-border-style: solid; -fx-border-color: black; -fx-border-radius: 3px;");
        this.text.setStyle("-fx-font-size: 22px; -fx-effect: dropshadow( three-pass-box, white , 3.0, 0.8,0.0,0.0);");
        this.text.setAlignment(Pos.CENTER);
        this.getChildren().add(text);
        AnchorPane.setTopAnchor(text,0.0);
        AnchorPane.setRightAnchor(text,0.0);
        AnchorPane.setBottomAnchor(text,0.0);
        AnchorPane.setLeftAnchor(text,0.0);
    }
}
