package lsg.graphics;

import lsg.armor.MetalChestPlate;
import lsg.bags.Collectible;
import lsg.consumables.drinks.SmallStamPotion;
import lsg.consumables.food.SuperBerry;
import lsg.helper.Dice;
import lsg.weapons.MetalSword;

import java.util.ArrayList;
import java.util.List;

public class LootSystem {
    public static Collectible randomLoot(){
        List<Collectible> collectibles = new ArrayList<>();
        collectibles.add(new SmallStamPotion());
        collectibles.add(new SuperBerry());
        collectibles.add(new MetalChestPlate());
        collectibles.add(new MetalSword());
        return collectibles.get(new Dice(collectibles.size()).roll());
    }
}
