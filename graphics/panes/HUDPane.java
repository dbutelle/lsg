package lsg.graphics.panes;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import lsg.graphics.widgets.DisplayButton;
import lsg.graphics.widgets.characters.statbars.StatBar;
import lsg.graphics.widgets.skills.SkillBar;
import lsg.graphics.widgets.texts.GameLabel;

public class HUDPane extends BorderPane {
    private MessagePane messagePane;
    private StatBar heroStatBar;
    private StatBar monsterStatBar;
    private SkillBar skillBar;
    private IntegerProperty score = new SimpleIntegerProperty();
    private GameLabel scoreLabel;

    private DisplayButton armor;
    private VBox armorDisplayed;
    private DisplayButton bag;
    private VBox bagDisplayed;
    private VBox lootBox;

    public HUDPane(){
        buildCenter();
        buildTop();
        buildBottom();
        buildLeft();
        buildRight();
        score.addListener((ChangeListener) (observable, oldValue, newValue) -> {
            scoreLabel.setText(""+score.getValue());
        });
    }

    public MessagePane getMessagePane() {
        return messagePane;
    }

    public StatBar getHeroStatBar() {
        return heroStatBar;
    }

    public StatBar getMonsterStatBar() {
        return monsterStatBar;
    }

    public SkillBar getSkillBar() {
        return skillBar;
    }

    public DisplayButton getArmorButton() {
        return armor;
    }

    public VBox getArmorDisplayed() {
        return armorDisplayed;
    }

    public DisplayButton getBagButton() {
        return bag;
    }

    public VBox getBagDisplayed() {
        return bagDisplayed;
    }

    public VBox getLootBox() { return lootBox; }

    public IntegerProperty scoreProperty(){return score;}

    private void buildCenter(){
        messagePane = new MessagePane();
        this.setCenter(messagePane);
    }

    private void buildTop(){
        BorderPane borderPane = new BorderPane();
        borderPane.setPadding(new Insets(70,10,0,10));
        this.setTop(borderPane);
        heroStatBar = new StatBar();
        borderPane.setLeft(heroStatBar);
        monsterStatBar = new StatBar();
        monsterStatBar.flip();
        borderPane.setRight(monsterStatBar);
        scoreLabel = new GameLabel("0");
        scoreLabel.setScaleX(1.3);
        scoreLabel.setScaleY(1.3);
        scoreLabel.setTranslateY(40);
        borderPane.setCenter(scoreLabel);
    }

    private void buildBottom(){
        skillBar = new SkillBar();
        this.setBottom(skillBar);
    }

    private void buildLeft(){
        BorderPane borderPane = new BorderPane();
        borderPane.setPrefWidth(250);
        borderPane.setMaxHeight(Double.MAX_VALUE);
        borderPane.setPadding(new Insets(10,20,10,20));
        borderPane.setStyle("-fx-border-style: solid; -fx-border-color: black; -fx-border-radius: 3px;");
        this.setLeft(borderPane);
        armor = new DisplayButton("Display Armor");
        borderPane.setTop(armor);
        armorDisplayed = new VBox();
        armorDisplayed.setPadding(new Insets(10,0,10,0));
        armorDisplayed.setSpacing(5);
        borderPane.setCenter(armorDisplayed);
    }

    private void buildRight(){
        BorderPane borderPane = new BorderPane();
        borderPane.setPrefWidth(250);
        borderPane.setMaxHeight(Double.MAX_VALUE);
        borderPane.setPadding(new Insets(10,20,10,20));
        borderPane.setStyle("-fx-border-style: solid; -fx-border-color: black; -fx-border-radius: 3px;");
        this.setRight(borderPane);
        bag = new DisplayButton("Display Bag");
        borderPane.setTop(bag);
        bagDisplayed = new VBox();
        bagDisplayed.setPadding(new Insets(10,0,10,0));
        bagDisplayed.setSpacing(5);
        lootBox = new VBox();
        lootBox.setPadding(new Insets(10,0,10,0));
        lootBox.setSpacing(5);
        borderPane.setCenter(bagDisplayed);
        borderPane.setRight(lootBox);
    }
}
