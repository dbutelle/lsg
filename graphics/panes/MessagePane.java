package lsg.graphics.panes;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.TranslateTransition;
import javafx.geometry.Pos;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import lsg.graphics.widgets.texts.GameLabel;

public class MessagePane extends VBox {
    private static final Duration ANIMATION_DURATION = Duration.millis(3000);
    public MessagePane(){
        setAlignment(Pos.CENTER);
    }

    public void showMessage(String msg){
        GameLabel gameLabel = new GameLabel(msg);
        this.getChildren().add(gameLabel);

        TranslateTransition tt = new TranslateTransition(ANIMATION_DURATION);
        tt.setToY(this.getHeight() * -0.2);

        FadeTransition ft = new FadeTransition(ANIMATION_DURATION) ;
        ft.setFromValue(1);
        ft.setToValue(0);

        ParallelTransition pt = new ParallelTransition(tt,ft);
        pt.setNode(this);
        pt.setCycleCount(1);
        pt.setOnFinished((event)->{
            this.getChildren().remove(gameLabel);
        });
        pt.play();
    }
}
