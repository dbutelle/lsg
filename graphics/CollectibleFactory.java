package lsg.graphics;

import javafx.scene.image.Image;
import lsg.armor.MetalChestPlate;
import lsg.bags.Collectible;
import lsg.consumables.drinks.SmallStamPotion;
import lsg.consumables.food.SuperBerry;
import lsg.weapons.MetalSword;


public class CollectibleFactory {

    public static Image getImageFor(Collectible collectible){
        if(collectible instanceof SmallStamPotion){
            return ImageFactory.getSprites(ImageFactory.SPRITES_ID.SMALL_STAM_POTION)[0];
        }
        if(collectible instanceof SuperBerry){
            return ImageFactory.getSprites(ImageFactory.SPRITES_ID.SUPER_BERRY)[0];
        }
        if(collectible instanceof MetalChestPlate){
            return ImageFactory.getSprites(ImageFactory.SPRITES_ID.METAL_CHEST_PLATE)[0];
        }
        if(collectible instanceof MetalSword){
            return ImageFactory.getSprites(ImageFactory.SPRITES_ID.METAL_SWORD)[0];
        }
        return null;
    }
}
