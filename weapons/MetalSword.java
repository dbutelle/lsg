package lsg.weapons;

public class MetalSword extends Weapon{
    private static String NAME = "Metal Sword" ;

    public MetalSword() {
        super(NAME, 7, 15, 20, 150) ;
    }
}
