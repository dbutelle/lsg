package lsg.armor;

public class MetalChestPlate extends ArmorItem{

        private static String ARMOR_NAME = "Metal Chest Plate" ;
        private static float ARMOR_VALUE = 14.99f ;

        public MetalChestPlate() {
            super(ARMOR_NAME, ARMOR_VALUE) ;
        }

}
